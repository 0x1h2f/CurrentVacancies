﻿using Microsoft.Extensions.DependencyInjection;

namespace CurrentVacancies.Services
{
    public static class ServiceProviderExtensions
    {
        public static void AddHhruVacanciesGetterService(this IServiceCollection services)
        {
            services.AddTransient<IVacanciesGetter, HhruVacanciesGetter>();
        }
    }
}
