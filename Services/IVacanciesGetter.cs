﻿using CurrentVacancies.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CurrentVacancies.Services
{
    public interface IVacanciesGetter
    {
        Task<List<Vacancy>> GetVacanciesAsync(bool fromDB);
        Task<Vacancy> GetVacancyAsync(int id, bool fromDB);
    }
}
