﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using CurrentVacancies.Models;
using CurrentVacancies.Models.Hh;
using System.IO;
using Newtonsoft.Json;

namespace CurrentVacancies.Services
{
    public class HhruVacanciesGetter : IVacanciesGetter
    {
        private readonly VacanciesContext db;

        public HhruVacanciesGetter(VacanciesContext context)
        {
            db = context;
        }
        
        public async Task<List<Vacancy>> GetVacanciesAsync(bool fromDB)
        {
            List<Vacancy> vacancies = null;

            try
            {
                if (fromDB)
                {
                    vacancies = db.Vacancies.Take(50).ToList();
                }
                else
                {
                    string jsonString = await Request("https://api.hh.ru/vacancies/?per_page=50");

                    if (jsonString != "")
                    {
                        vacancies = new List<Vacancy>();

                        foreach (HhVacancy hhvacancy in JsonConvert.DeserializeObject<HhResponse>(jsonString).items)
                        {
                            vacancies.Add(hhvacancy.toVacancy());
                        }

                        foreach (Vacancy vacancy in vacancies)
                        {
                            if (db.Vacancies.Any(p => p.Id == vacancy.Id))
                            {
                                Vacancy _vacancy = db.Vacancies.FirstOrDefault(p => p.Id == vacancy.Id);

                                _vacancy.Id = vacancy.Id;
                                _vacancy.Name = vacancy.Name;
                                _vacancy.Address = vacancy.Address;
                                _vacancy.Salary = vacancy.Salary;
                                _vacancy.PublishedAt = vacancy.PublishedAt;
                                _vacancy.Employer = vacancy.Employer;
                                _vacancy.ShortDescription = vacancy.ShortDescription;
                                _vacancy.Contacts = vacancy.Contacts;

                                db.Update(_vacancy);
                            }
                            else
                            {
                                db.Add(vacancy);
                            }
                        }
                        db.SaveChanges();
                    } 
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return vacancies;
        }

        public async Task<Vacancy> GetVacancyAsync(int id, bool fromDB)
        {
            Vacancy vacancy = null;

            try
            {
                if (fromDB)
                {
                    vacancy = db.Vacancies.FirstOrDefault(p => p.Id == id.ToString());
                }
                else
                {
                    string jsonString = await Request($"https://api.hh.ru/vacancies/{id}");
                    if (jsonString != "")
                    {
                        vacancy = JsonConvert.DeserializeObject<HhVacancy>(jsonString).toVacancy();

                        if (db.Vacancies.Any(p => p.Id == vacancy.Id))
                        {
                            Vacancy _vacancy = db.Vacancies.FirstOrDefault(p => p.Id == vacancy.Id);

                            _vacancy.Id = vacancy.Id;
                            _vacancy.Name = vacancy.Name;
                            _vacancy.Address = vacancy.Address;
                            _vacancy.Salary = vacancy.Salary;
                            _vacancy.PublishedAt = vacancy.PublishedAt;
                            _vacancy.Employer = vacancy.Employer;
                            _vacancy.Description = vacancy.Description;
                            _vacancy.Employment = vacancy.Employment;
                            _vacancy.Experience = vacancy.Experience;

                            db.Update(_vacancy);
                        }
                        else
                        {
                            db.Add(vacancy);
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return vacancy;
        }

        private class HhResponse
        {
            public List<HhVacancy> items { get; set; }
        }

        private async Task<string> Request(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers["User-Agent"] = "CurrentVacancies/1.0 (hafizov_fadis@mail.ru)";

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                return reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (WebException e)
            {
                return "";
            }

            return "";
        }
    }
}
