/// <binding BeforeBuild='default' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var webpack = require('gulp-webpack');

gulp.task('default', function () {
    return gulp.src('./wwwroot/app/app.jsx')
        .pipe(webpack({
            entry: "./wwwroot/app/app.jsx",
            output: {
                filename: "bundle.js"
            },
            resolve: {
                extensions: ["", ".js", ".jsx"]
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        exclude: /(node_modules)/,
                        loader: ["babel-loader"],
                        query: {
                            presets: ["es2015", "react"]
                        }
                    }
                ]
            }
        }))
        .pipe(gulp.dest('./wwwroot/public/'));
});