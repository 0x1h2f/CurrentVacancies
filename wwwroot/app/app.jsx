import ReactDOM from 'react-dom'
import React from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import Main from './components/Main'
import VacancyUrl from './components/VacancyUrl'
import VacanciesList from './components/VacanciesList'
import NotFound from './components/NotFound'

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={Main}>
            <Route path=":id" component={VacancyUrl} />
            <IndexRoute component={VacanciesList} />
        </Route>

        <Route path="*" component={NotFound} />
    </Router>,
    document.getElementById("container")
)
