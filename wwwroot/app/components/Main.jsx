import React from 'react'
import { Link } from 'react-router'

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.onFromDbCheckboxChange = this.onFromDbCheckboxChange.bind(this);
    this.onSearchInputChange = this.onSearchInputChange.bind(this);
  }

  onFromDbCheckboxChange(e) {
    this.setState({ fromDB: e.target.checked});
  }

  onSearchInputChange(e) {
    this.setState({ searchText: e.target.value});
  }

  render() {
    var fromDB = (this.state && this.state.fromDB ? this.state.fromDB : false);
    var searchText = (this.state && this.state.searchText ? this.state.searchText : "");
    const childrenWithProps = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        fromDB: fromDB,
        searchText: searchText
      })
    );

    return  <div>
              <nav className="navbar navbar-default" role="navigation">
                <div className="container-fluid">
                  <div className="navbar-header">
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                      <span className="sr-only">Toggle navigation</span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                    </button>
                    <Link to="/" className="navbar-brand">Актуальные вакансии</Link>
                  </div>

                  <div className="collapse navbar-collapse" id="navbar">
                    {
                      !this.props.params.id
                      ? <ul className="nav navbar-nav navbar-left">
                          <li>
                            <a style={{padding: '10px'}}>
                              <input type="text" className="form-control" onChange={this.onSearchInputChange} value={searchText} placeholder="Поиск"/>
                            </a>
                          </li>
                        </ul>
                      : ""
                    }
                    <ul className="nav navbar-nav navbar-right">
                      <li>
                        <a>
                          <input type="checkbox" onChange={this.onFromDbCheckboxChange} checked={fromDB} /> ИЗ БАЗЫ
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>

              {childrenWithProps}
            </div>;
  }
}

module.exports = Main;
