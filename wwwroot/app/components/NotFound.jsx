import React from 'react'
import { Link } from 'react-router'

class NotFound extends React.Component {
  render() {
    return  <div className="jumbotron">
              <h1>{this.props.title}</h1>
              <p>{this.props.description}</p>
              <Link to="/" className="btn btn-primary btn-lg">{this.props.text}</Link>
            </div>;
  }
}

NotFound.defaultProps = {
  title: "404",
  description: "Ресурс не найден",
  text: "На главную"
};

module.exports = NotFound;
