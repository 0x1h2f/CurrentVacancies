import React from 'react'
import { Link } from 'react-router'
import NotFound from './NotFound'

class VacancyData extends React.Component {
  render() {
    var data = this.props.vacancy;
    if (data) {
      return  <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title"><Link to={`/${data.id}`} dangerouslySetInnerHTML={{ __html: data.name}} /></h3>
                </div>
                <div className="panel-body">

                  {data.salary ? <p><strong>Уровень зарплаты:</strong> {data.salary}</p> : ""}
                  {data.experience ? <p><strong>Требуемый опыт работы:</strong> {data.experience}</p> : ""}
                  <p dangerouslySetInnerHTML={{ __html: data.shortDescription}} />
                </div>
                <div className="panel-footer">
                  {data.employer ? <p><strong>{data.employer}</strong></p> : ""}
                  {data.publishedAt ? <p><strong>Дата публикации вакансии:</strong> {data.publishedAt}</p> : ""}
                </div>
              </div>;
    }
    else {
      return <NotFound title=":(" description="Вакансия не найдена" />;
    }
  }
}

module.exports = VacancyData;
