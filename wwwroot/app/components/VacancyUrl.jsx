import React from 'react'
import NotFound from './NotFound'

const apiUrl = "api/vacancies";

class VacancyUrl extends React.Component {
  loadData(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("get", apiUrl + "/" + id + "/" + this.props.fromDB, true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        var data = JSON.parse(xhr.responseText);
        this.setState({ data: data });
      }
    }.bind(this);
    xhr.send();
  }

  componentDidMount() {
    this.setState({ fromDB: this.props.fromDB });
    var vacancyId = this.props.params.id;
    if (vacancyId && !isNaN(parseInt(vacancyId))) {
      vacancyId = parseInt(vacancyId);
      this.loadData(vacancyId);
    }
  }

  componentDidUpdate() {
    if (this.state.fromDB != this.props.fromDB) {
      this.setState({ fromDB: this.props.fromDB });
      var vacancyId = this.props.params.id;
      if (vacancyId && !isNaN(parseInt(vacancyId))) {
        vacancyId = parseInt(vacancyId);
        this.loadData(vacancyId);
      }
    }
  }

  render() {
    var vacancyId = this.props.params.id;
    if (vacancyId && !isNaN(parseInt(vacancyId))) {
      if (this.state && this.state.data) {
        var data = this.state.data;
        return  <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">{data.name}</h3>
                  </div>
                  <div className="panel-body">

                    {data.salary ? <p><strong>Уровень зарплаты:</strong> {data.salary}</p> : ""}
                    {data.experience ? <p><strong>Требуемый опыт работы:</strong> {data.experience}</p> : ""}
                    <p dangerouslySetInnerHTML={{ __html: data.description}} />
                    {data.employment ? <p><strong>Тип занятости:</strong> {data.employment}</p> : ""}
                  </div>
                  <div className="panel-footer">
                    {data.employer ? <p><strong>{data.employer}</strong></p> : ""}
                    {data.address ? <p>{data.address}</p> : ""}
                    {data.contacts ? <p dangerouslySetInnerHTML={{ __html: data.contacts}} /> : ""}
                    {data.publishedAt ? <p><strong>Дата публикации вакансии:</strong> {data.publishedAt}</p> : ""}
                  </div>
                </div>;
      }
      else {
        return <NotFound title=":(" description="Вакансия не найдена" />
      }
    }
    else {
      return <NotFound />
    }
  }
}

module.exports = VacancyUrl;
