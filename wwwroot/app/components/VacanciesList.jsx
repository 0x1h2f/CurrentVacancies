import React from 'react'
import NotFound from './NotFound'
import VacancyData from './VacancyData'

const apiUrl = "api/vacancies";

class VacanciesList extends React.Component {
  search(text) {
    var filteredData = JSON.parse(JSON.stringify(this.state.data));
    filteredData = filteredData.filter(function(vacancy){
      return vacancy.name.toLowerCase().search(text.toLowerCase())!== -1;
    });

    if (text) {
      filteredData = filteredData.map(function(vacancy){
        vacancy.name = vacancy.name.replace(new RegExp("(" + text + ")", 'gi'), "<mark>$1</mark>");
        return vacancy;
      });
    }
    
    return filteredData;
  }

  loadData() {
    var xhr = new XMLHttpRequest();
    xhr.open("get", apiUrl + "/" + this.props.fromDB, true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        var data = JSON.parse(xhr.responseText);
        this.setState({ data: data });
        this.setState({filteredData: this.search(this.props.searchText)});
      }
    }.bind(this);
    xhr.send();
  }

  componentDidMount() {
    this.setState({ fromDB: this.props.fromDB });
    this.setState({ searchText: this.props.searchText });
    this.loadData();
  }

  componentDidUpdate() {
    if (this.state.fromDB != this.props.fromDB) {
      this.setState({ fromDB: this.props.fromDB });
      this.loadData();
    }
    if (this.state.searchText != this.props.searchText) {
      this.setState({ searchText: this.props.searchText });
      this.setState({filteredData: this.search(this.props.searchText)});
    }
  }

  render() {
    if (this.state && this.state.filteredData) {
      var data = this.state.filteredData;
      return  <div>
                {
                  data.map(function(vacancy){
                    return <VacancyData key={vacancy.id} vacancy={vacancy} />
                  })
                }
              </div>;
    }
    else {
      return <NotFound title=":(" description="Вакансий не найдено" text="Обновить" />
    }
  }
}

module.exports = VacanciesList;
