﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CurrentVacancies.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vacancies",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Contacts = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Employer = table.Column<string>(nullable: true),
                    Employment = table.Column<string>(nullable: true),
                    Experience = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PublishedAt = table.Column<string>(nullable: true),
                    Salary = table.Column<string>(nullable: true),
                    ShortDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacancies", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vacancies");
        }
    }
}
