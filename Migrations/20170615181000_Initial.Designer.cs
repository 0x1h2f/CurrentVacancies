﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CurrentVacancies.Models;

namespace CurrentVacancies.Migrations
{
    [DbContext(typeof(VacanciesContext))]
    [Migration("20170615181000_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CurrentVacancies.Models.Vacancy", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Address");

                    b.Property<string>("Contacts");

                    b.Property<string>("Description");

                    b.Property<string>("Employer");

                    b.Property<string>("Employment");

                    b.Property<string>("Experience");

                    b.Property<string>("Name");

                    b.Property<string>("PublishedAt");

                    b.Property<string>("Salary");

                    b.Property<string>("ShortDescription");

                    b.HasKey("Id");

                    b.ToTable("Vacancies");
                });
        }
    }
}
