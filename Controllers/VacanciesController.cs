using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CurrentVacancies.Models;
using CurrentVacancies.Services;
using Microsoft.AspNetCore.Cors;

namespace CurrentVacancies.Controllers
{
    [Produces("application/json")]
    [Route("api/Vacancies")]
    [EnableCors("CorsPolicy")]
    public class VacanciesController : Controller
    {
        readonly object not_found_message = new { errors = new[] { new { type = "not_found" } } };

        IVacanciesGetter _hhruVacanciesGetter;

        public VacanciesController(IVacanciesGetter hhruVacanciesGetter)
        {
            _hhruVacanciesGetter = hhruVacanciesGetter;
        }

        // GET: api/vacancies
        [HttpGet("{fromDB:bool?}")]
        public async Task<IActionResult> GetVacancies(bool? fromDB)
        {
            List<Vacancy> vacancies = await _hhruVacanciesGetter.GetVacanciesAsync(fromDB.HasValue ? fromDB.Value : false);

            if (vacancies == null)
            {
                return NotFound(not_found_message);
            }

            return Ok(vacancies.ToList());
        }

        // GET: api/vacancies/{id:int}
        [HttpGet("{id:int}/{fromDB:bool?}")]
        public async Task<IActionResult> GetVacancy(int id, bool? fromDB)
        {
            Vacancy vacancy = await _hhruVacanciesGetter.GetVacancyAsync(id, fromDB.HasValue ? fromDB.Value : false);

            if (vacancy == null)
            {
                return NotFound(not_found_message);
            }

            return Ok(vacancy);
        }
    }
}