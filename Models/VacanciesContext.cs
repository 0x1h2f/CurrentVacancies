﻿using Microsoft.EntityFrameworkCore;

namespace CurrentVacancies.Models
{
    public class VacanciesContext :DbContext
    {
        public DbSet<Vacancy> Vacancies { get; set; }

        public VacanciesContext(DbContextOptions<VacanciesContext> options)
            : base(options)
        {
        }
    }
}
