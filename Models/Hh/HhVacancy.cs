﻿using System;

namespace CurrentVacancies.Models.Hh
{
    public class HhVacancy
    {
        //Идентификатор вакансии
        public string id { get; set; }

        //Описание вакансии, содержит html
        public string description { get; set; }

        //Брендированное описание вакансии (строка с кодом HTML)
        public string branded_description { get; set; }

        //Информация о ключевых навыках, заявленных в вакансии
        public class KeySkill
        {
            //название ключевого навыка
            public string name { get; set; }
        }

        public KeySkill[] key_skills { get; set; }

        //График работы
        public class Schedule
        {
            //Идентификатор графика работы
            public string id { get; set; }

            //Название графика работы
            public string name { get; set; }
        }

        public Schedule schedule { get; set; }

        //Указание, что вакансия доступна для соискателей с инвалидностью
        public bool accept_handicapped { get; set; }

        //Требуемый опыт работы
        public class Experience
        {
            //Идентификатор требуемого опыта работы
            public string id { get; set; }

            //Название требуемого опыта работы
            public string name { get; set; }
        }

        public Experience experience { get; set; }

        //Адрес вакансии
        public class Address
        {
            //Город
            public string city { get; set; }

            //Улица
            public string street { get; set; }

            //Номер дома
            public string building { get; set; }

            //Дополнительная информация об адресе
            public string description { get; set; }

            //Географическая широта
            public decimal? lat { get; set; }

            //Географическая долгота
            public decimal? lng { get; set; }

            //Текстовое описание адреса, как было введено
            public string raw { get; set; }

            //Список станций метро
            public class MetroStation
            {
                //Идентификатор станции метро
                public string station_id { get; set; }

                //Название станции метро
                public string station_name { get; set; }

                //Идентификатор линии метро, на которой находится станция
                public string line_id { get; set; }

                //Название линии метро, на которой находится станция
                public string line_name { get; set; }

                //Географическая широта станции метро
                public decimal? lat { get; set; }

                //Географическая долгота станции метро
                public decimal? lng { get; set; }
            }

            public MetroStation[] metro_stations { get; set; }
        }

        public Address address { get; set; }

        //Ссылка на представление вакансии на сайте
        public string alternate_url { get; set; }

        //Ссылка на отклик на вакансию на сайте
        public string apply_alternate_url { get; set; }

        //Внутренний код вакансии работадателя
        public string code { get; set; }

        //Департамент, от имени которого размещается вакансия
        public class Department
        {
            //Идентификатор департамента
            public string id { get; set; }

            //Название департамента
            public string name { get; set; }
        }

        public Department department { get; set; }

        //Тип занятости
        public class Employment
        {
            //Идентификатор типа занятости
            public string id { get; set; }

            //Название типа занятости
            public string name { get; set; }
        }

        public Employment employment { get; set; }

        //Оклад
        public class Salary
        {
            //Нижняя граница вилки оклада
            public decimal? from { get; set; }

            //Верняя граница вилки оклада
            public decimal? to { get; set; }

            //Идентификатор валюты оклада
            public string currency { get; set; }
        }

        public Salary salary { get; set; }

        //Находится ли данная вакансия в архиве
        public bool archived { get; set; }

        //Название вакансии
        public string name { get; set; }

        //Регион размещения вакансии
        public class Area
        {

            //Идентификатор региона
            public string id { get; set; }

            //Название региона
            public string name { get; set; }

            //Url получения информации о регионе
            public string url { get; set; }
        }

        public Area area { get; set; }

        //Дата и время публикации вакансии
        public string published_at { get; set; }

        //Короткое представление работодателя
        public class Employer
        {
            //идентификатор работодателя
            public string id { get; set; }

            //название работодателя
            public string name { get; set; }

            //url для получения полного описания работодателя
            public string url { get; set; }

            //ссылка на описание работодателя на сайте
            public string alternate_url { get; set; }

            //url для получения поисковой выдачи с вакансиями данной компании
            public string vacancies_url { get; set; }

            //количество открытых вакансий у работодателя
            public decimal? open_vacancies { get; set; }

            //флаг, указывает на то, что компанию прошла проверку на сайте.
            public bool trusted { get; set; }

            //Добавлены ли все вакансии работодателя в список скрытых
            public bool blacklisted { get; set; }
        }

        public Employer employer { get; set; }

        //Обязательно ли заполнять сообщение при отклике на вакансию
        public bool response_letter_required { get; set; }

        //Тип вакансии
        public class Type
        {
            //Идентификатор типа вакансии
            public string id { get; set; }

            //Название типа вакансии
            public string name { get; set; }
        }

        public Type type { get; set; }

        //На вакансии с типом direct нельзя откликнуться на сайте hh.ru, у этих вакансий в ключе response_url выдаётся URL внешнего сайта (чаще всего это сайт работодателя с формой отклика).
        public string response_url { get; set; }

        //Информация о прикрепленном тестовом задании к вакансии.В случае отсутствия теста — null. В данный момент отклик на вакансии с обязательным тестом через API невозможен.
        public class Test
        {
            //Обязательно ли заполнение теста для отклика
            public bool required { get; set; }
        }

        public Test test { get; set; }

        //Специализации
        public class Specialization
        {
            //Идентификатор специализации
            public string id { get; set; }

            //Название специализации
            public string name { get; set; }

            //Идентификатор профессиональной области, в которую входит специализация
            public string profarea_id { get; set; }

            //Название профессиональной области, в которую входит специализация
            public string profarea_name { get; set; }
        }

        public Specialization[] specialization { get; set; }

        //Контактная информация.В вакансиях, где контакты не указаны, возвращается null.
        public class Contact
        {
            //Имя контактного лица
            public string name { get; set; }

            //Email контактного лица
            public string email { get; set; }

            //Список телефонов контактного лица. Может быть пустым.
            public class Phone
            {
                //Код страны
                public string country { get; set; }

                //Код города
                public string city { get; set; }

                //Номер телефона
                public string number { get; set; }

                //Комментарий
                public string comment { get; set; }
            }

            public Phone[] phones { get; set; }
        }

        public Contact contacts { get; set; }

        //Биллинговый тип вакансии
        public class BillingType
        {
            //Идентификатор биллингового типа вакансии
            public string id { get; set; }

            //Название биллингового типа вакансии
            public string name { get; set; }
        }

        public BillingType billing_type { get; set; }

        //Включена ли возможность соискателю писать сообщения работодателю, после приглашения/отклика на вакансию
        public bool allow_messages { get; set; }

        //Является ли данная вакансия премиум-вакансией
        public bool premium { get; set; }

        //Дополнительные текстовые снипеты по найденной вакансии
        public class Snippet
        {
            //Требования по вакансии, если они найдены в тексте описания.
            public string requirement { get; set; }
            //Обязанности по вакансии, если они найдены в тексте описания.
            public string responsibility { get; set; }
        }

        public Snippet snippet { get; set; }

        public Vacancy toVacancy()
        {
            Vacancy vacancy = new Vacancy();

            vacancy.Id = this.id;
            vacancy.Name = this.name;
            vacancy.Description = (this.description != null ? this.description : "");

            vacancy.Employment = "";
            if (this.employment != null && this.employment.name != null && this.employment.name != "")
            {
                vacancy.Employment = this.employment.name;
            }
            if (this.schedule != null && this.schedule.name != null && this.schedule.name != "")
            {
                if (vacancy.Employment != "")
                {
                    vacancy.Employment += ", " + this.schedule.name;
                }
                else
                {
                    vacancy.Employment = this.schedule.name;
                }
            }

            vacancy.Experience = (this.experience != null && this.experience.name != null ? this.experience.name : "");

            vacancy.Address = "";
            if (this.address != null)
            {
                if (this.address.city != null && this.address.city != "")
                {
                    vacancy.Address = this.address.city;
                }
                if (this.address.street != null && this.address.street != "")
                {
                    if (vacancy.Address != "")
                    {
                        vacancy.Address += ", " + this.address.street;
                    }
                    else
                    {
                        vacancy.Address = this.address.street;
                    }
                }
                if (this.address.building != null && this.address.building != "")
                {
                    if (vacancy.Address != "")
                    {
                        vacancy.Address += ", " + this.address.building;
                    }
                    else
                    {
                        vacancy.Address = this.address.building;
                    }
                }
                if (this.address.description != null && this.address.description != "")
                {
                    if (vacancy.Address != "")
                    {
                        vacancy.Address += " (" + this.address.description + ")";
                    }
                    else
                    {
                        vacancy.Address = this.address.description;
                    }
                }
            }

            vacancy.Salary = "";
            if (this.salary != null)
            {
                if (this.salary.from.HasValue && this.salary.from.Value != 0)
                {
                    vacancy.Salary = "От " + this.salary.from.ToString();
                }
                if (this.salary.to.HasValue && this.salary.to.Value != 0)
                {
                    if (vacancy.Salary != "")
                    {
                        vacancy.Salary += " до " + this.salary.to.ToString();
                    }
                    else
                    {
                        vacancy.Salary = "До " + this.salary.to.ToString();
                    }
                }
                if (this.salary.currency != null && this.salary.currency != "")
                {
                    if (vacancy.Salary != "")
                    {
                        vacancy.Salary += " " + this.salary.currency;
                    }
                }
            }

            vacancy.PublishedAt = (this.published_at != null && this.published_at != "" ? DateTime.Parse(this.published_at).ToString("dd/MM/yyyy HH:mm:ss") : "");

            vacancy.Employer = (this.employer != null && this.employer.name != null ? this.employer.name : "");

            vacancy.Contacts = "";
            if (this.contacts != null)
            {
                if (this.contacts.name != null && this.contacts.name != "")
                {
                    vacancy.Contacts = this.contacts.name + ".";
                }
                if (this.contacts.email != null && this.contacts.email != "")
                {
                    if (vacancy.Contacts != "")
                    {
                        vacancy.Contacts += " <strong>Почта:</strong> " + this.contacts.email + ".";
                    }
                    else
                    {
                        vacancy.Contacts = "<strong>Почта:</strong> " + this.contacts.email + ".";
                    }
                }

                if (this.contacts.phones.Length > 0)
                {
                    vacancy.Contacts += " <strong>Телефон:</strong> ";
                }
                int i = 0;
                foreach (var phone in this.contacts.phones)
                {
                    if (phone.country != null && phone.country != "")
                    {
                        vacancy.Contacts += "+" + phone.country;
                    }
                    if (phone.city != null && phone.city != "")
                    {
                        vacancy.Contacts += " (" + phone.city + ") ";
                    }
                    if (phone.number != null && phone.number != "")
                    {
                        vacancy.Contacts += phone.number;
                    }
                    if (i < this.contacts.phones.Length - 1)
                    {
                        vacancy.Contacts += ", ";
                    }
                    i++;
                }
            }

            vacancy.ShortDescription = "";
            if (this.snippet != null)
            {
                if (this.snippet.requirement != null && this.snippet.requirement != "")
                {
                    vacancy.ShortDescription = "<strong>Требования:</strong> " + this.snippet.requirement + ".";
                }
                if (this.snippet.responsibility != null && this.snippet.responsibility != "")
                {
                    if (vacancy.Contacts != "")
                    {
                        vacancy.ShortDescription += " <strong>Обязанности:</strong> " + this.snippet.responsibility + ".";
                    }
                    else
                    {
                        vacancy.ShortDescription = "<strong>Обязанности:</strong> " + this.snippet.responsibility + ".";
                    }
                }
            }

            return vacancy;
        }
    }
}
