﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrentVacancies.Models
{
    public class Vacancy
    {
        //идентификатор вакансии
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        //название вакансии
        public string Name { get; set; }

        //описание вакансии
        public string Description { get; set; }

        //краткое описание вакансии
        public string ShortDescription { get; set; }

        //тип занятости
        public string Employment { get; set; }

        //опыт работы
        public string Experience { get; set; }

        //адрес вакансии
        public string Address { get; set; }

        //оклад
        public string Salary { get; set; }

        //дата и время публикации
        public string PublishedAt { get; set; }

        //работодатель
        public string Employer { get; set; }

        //контакты
        public string Contacts { get; set; }
    }
}
